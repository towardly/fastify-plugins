/// <reference types="node" />
import { FastifyPluginCallback } from 'fastify';

declare module 'fastify' {
  interface fastifyMongoose {
    /** 所有的 BookShelf Model 集 */
    models: {
      /** 获取某一个 BookShelf Model */
      [index: string]: typeof BookShelf.Model;
    };
  }
}

/**
 * 配置项
 */
interface FastifyMongooseOptions {
  /**
   * 配置连接的数据库地址
   */
  url: string;
  /** mongoose 的 connect options: https://mongoosejs.com/docs/connections.html */
  settings?: object;
  /** 定义 mongoose model:  */
  models?: { alias?: string; schema: object; options?: object; name: string }[];
}

declare const fastifyMongoose: FastifyPluginCallback<NonNullable<FastifyMongooseOptions>>;

export default fastifyMongoose;
export { fastifyMongoose };
