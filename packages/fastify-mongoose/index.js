const fp = require('fastify-plugin');
const mongoose = require('mongoose');

function MongoosePlugin(fastify, opts, next) {
  // 避免插件重复加载
  if (!fastify.mongo != null) {
    next(new Error('fastify-mongoose is already register!'));
    return;
  }
  /* 连接 mongodb */
  const connectOptions = { ...opts.settings };
  if (!opts.uri) {
    next(new Error('`uri` parameter is mandatory'));
    return;
  }
  mongoose.connect(opts.uri, connectOptions);
  mongoose.connection.once('open', () => {
    fastify.log.info('Connected to mongodb success!');
  });
  mongoose.connection.on('error', (err) => {
    fastify.log.error(err);
  });
  mongoose.connection.on('disconnected', () => {
    fastify.log.warn('mongodb disconnected!');
  });
  const decorator = {
    instance: mongoose.connection,
  };
  /* 定义 model */
  if (opts.models && opts.models.length > 0) {
    for (let i = 0, len = opts.models.length; i < len; i++) {
      let model = opts.models[i];
      const schema = new mongoose.Schema(model.schema, model.options || {});
      decorator[model.name] = mongoose.model(model.name, schema, model.connName);
    }
  }
  // Close connection when app is closing
  fastify.addHook('onClose', (app, done) => {
    app.mongo.instance.close();
    done();
  });

  fastify.decorate('mongo', decorator);
  next();
}

module.exports = fp(MongoosePlugin, { fastify: '3.x', name: 'fastify-mongoose' });
