## fastify-mongoose

---

在 _fastify_ 中使用 _mongoose_ 连接 _MongoDB_ 数据库。

### 安装

```shell
npm install fastify-mongoose-next
```

### 使用

```javascript
// 配置连接
fastify.register(require('fastify-mongoose-next'), {
  uri: 'mongodb://admin:123asd@127.0.0.1:27017/accounts?authSource=admin&authMechanism=SCRAM-SHA-1', // 连接数据库的地址
  settings: {},
  models: [
    {
      name: 'Tank',
      schema: { title: String },
      options: {},
    },
  ],
});

// 使用
fastify.get('/system_monitor', async (req, reply) => {
  // 在这里通过 fastify.mongo.Tank 取到构建的 Model
  await fastify.mongo.Tank.findOne();
  reply.send('SUCCESS');
});
```

### 配置说明

1. `uri`: `String`，_必填_, 定义连接到 `MongoDB` 的地址，详情参考：[mongoose-connect](https://mongoosejs.com/docs/connections.html)
2. `settings`: `Object`，_选填_，数据库连接选项，具体请参考：[mongoose-connect-options](https://mongoosejs.com/docs/connections.html#options)
3. `models`: `Object`，_必填_，配置 `mongoose-model`

   - `schema`: `Object`，定义 `mongoose-schema`，形式同: [mongoose-define-schema](https://mongoosejs.com/docs/guide.html#definition)
   - `options`: `Object`，_选填_，定义 `schema` 的配置选项，[schemas-options](https://mongoosejs.com/docs/guide.html#options)
   - `name`: `String`，模块的名称，也就是 `mongoose.model()` 的第一个参数，同时也作为访问名称例如：`fastify.mongo.x` 这里的 `x` 就是传递的 `name`
   - `connName`: `String`，可选，填写对应的 `MongoDB connection` 名称，也就是 `mongoose.model()` 的第3个参数：[mongoose_Mongoose-model](https://mongoosejs.com/docs/api/mongoose.html#mongoose_Mongoose-model)
