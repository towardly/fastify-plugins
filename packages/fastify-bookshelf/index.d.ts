/// <reference types="node" />
import BookShelf = require('bookshelf');
import { Knex } from 'knex';
import { FastifyPluginCallback } from 'fastify';

declare module 'fastify' {
  interface FastifyInstance {
    /** 所有的 BookShelf Model 集 */
    models: {
      /** 获取某一个 BookShelf Model */
      [index: string]: typeof BookShelf.Model;
    };
  }
}

/**
 * 配置项
 */
interface FastifyBookshelfOptions {
  /**
   * 配置连接的数据库，如果不填，默认为：mysql2
   */
  client?: string;
  /** 数据库连接地址 */
  connection?: string | Knex.StaticConnectionConfig | Knex.ConnectionConfigProvider;
  /** 所有的数据库表对应的model，string 使用的表名则为 model首字母小写，末尾加 's', object 则为 bookshelf 配置的基础上增加 name 字段  */
  models: string[] | object[];
}

declare const fastifyBookshelf: FastifyPluginCallback<NonNullable<FastifyBookshelfOptions>>;

export default fastifyBookshelf;
export { fastifyBookshelf };
