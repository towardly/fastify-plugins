const fp = require('fastify-plugin');

function bookshelfPlugin(fastify, opts, next) {
  // 避免插件重复加载
  if (fastify.bookshelf != null) {
    next(new Error('fastify-bookshelf is already register!'));
    return;
  }
  const knex = require('knex')({
    client: opts.client || 'mysql2',
    connection: opts.connection,
    debug: true,
    log: {
      warn(msg) {
        fastify.log.warn(msg);
      },
      error(msg) {
        fastify.log.error(msg);
      },
      deprecate(msg) {
        fastify.log.info(msg);
      },
      debug(msg) {
        fastify.log.info(msg);
      },
    },
  });
  // 验证应用是否启动成功
  knex
    .raw('SELECT 1')
    .then(() => {
      fastify.log.info('Connection has been established successfully.');
    })
    .catch((err) => {
      fastify.log.error(err);
    });
  let inst = require('bookshelf')(knex);
  let models = {};
  if (opts.models != null && opts.models instanceof Array) {
    for (let model of opts.models) {
      if (typeof model === 'string') {
        models[model] = inst.model(model, {
          requireFetch: false,
          tableName: model[0].toLowerCase() + model.substr(1) + 's',
        });
      } else {
        const name = model.name;
        delete model.name;
        models[name] = inst.model(name, model);
      }
    }
  }
  fastify.decorate('models', models);
  next();
}

module.exports = fp(bookshelfPlugin, { fastify: '3.x', name: 'fastify-bookshelf' });
