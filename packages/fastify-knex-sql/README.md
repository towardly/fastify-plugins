# fastify-knex

`fastify` 中使用 [knex](https://knexjs.org/) 连接 `mysql` 数据库。

## 安装

```bash
yarn add fastify-knex

# 安装下面的其中一个，根据连接的数据库选择
yarn add pg
yarn add sqlite3
yarn add mysql
yarn add mysql2
yarn add mariasql
yarn add strong-oracle
yarn add oracle
yarn add mssql
```

## 使用

```javascript
const knex = require('fastify-knex');

fastify.register(knex, {
  client: 'mysql2',
  connection: 'mysql://usernmae:password@127.0.0.1:3306/database_name',
});

fastify.get('/', (request, reply) => {
  new fastify.knex.select();
});
```

该示例连接上数据库后，会在 `fastify` 实例上映射一个 `knex` 对象.

## 配置项

1. `client?: string`：使用的连接数据库的插件，需要进行 `npm install` 安装，详情参见：[knex 文档](http://knexjs.org/#Installation-client)；如果不填默认为：`mysql2`
2. `connection?: string | Knex.StaticConnectionConfig | Knex.ConnectionConfigProvider;`：数据库连接地址，[详情 knex 文档](http://knexjs.org/#Installation-client)

## 文档

连接项的配置参见 [knex](http://knexjs.org/#Installation-client)
