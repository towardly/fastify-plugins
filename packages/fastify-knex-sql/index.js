const fp = require('fastify-plugin');

function knexPlugin(fastify, opts, next) {
  // 避免插件重复加载
  if (fastify.knex != null) {
    next(new Error('fastify-knex is already register!'));
    return;
  }
  const knex = require('knex')({
    client: opts.client || 'mysql2',
    connection: opts.connection,
    debug: true,
    log: {
      warn(msg) {
        fastify.log.warn(msg);
      },
      error(msg) {
        fastify.log.error(msg);
      },
      deprecate(msg) {
        fastify.log.info(msg);
      },
      debug(msg) {
        fastify.log.info(msg);
      },
    },
  });
  // 验证应用是否启动成功
  knex
    .raw('SELECT 1')
    .then(() => {
      fastify.log.info('Connection has been established successfully.');
    })
    .catch((err) => {
      fastify.log.error(err);
    });

  // Close connection when app is closing
  fastify.addHook('onClose', (app, done) => {
    app.knex.destroy();
    done();
  });

  fastify.decorate('knex', knex);
  next();
}

module.exports = fp(knexPlugin, { fastify: '3.x', name: 'fastify-knex' });
