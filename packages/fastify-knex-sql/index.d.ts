/// <reference types="node" />
import { Knex } from 'knex';
import { FastifyPluginCallback } from 'fastify';

declare module 'fastify' {
  interface FastifyInstance {
    knex: Knex;
  }
}

/**
 * 配置项
 */
interface FastifyKnexOptions {
  /**
   * 配置连接的数据库，如果不填，默认为：mysql2
   */
  client?: string;
  /** 数据库连接地址 */
  connection?: string | Knex.StaticConnectionConfig | Knex.ConnectionConfigProvider;
}

declare const fastifyKnex: FastifyPluginCallback<NonNullable<FastifyKnexOptions>>;

export default fastifyKnex;
export { fastifyKnex };
