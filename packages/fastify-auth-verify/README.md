## fastify-auth-verify

---

`fastify` 登录校验，用于校验是否登录。

### 安装

```shell
npm install fastify-auth-verify
```

### 使用

```javascript
// 1. 注册校验, 加入登录验证
ChildServer.register(require('fastify-auth-verify'), {
  verify(req) {
    // 这里如果配合 fastify-pithy-session，这里可以返回 req.session.uid
    return 1;
  },
  ignore: ['/api/login', '/api/system_monitor', '/api/load', '/api/positions'],
});
```

### 配置介绍

1. `verify`: `function` 校验回调函数，参数 `req` 为 `fastify` 的请求的 `request`，如果该函数返回值为 `null` 则表明未登录，则会返回 [401 Unauthorized](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Status/401)。同时如果校验成功(已经登录)，则会把返回值保存到 `req.loginUser` 里面，在路由接口处理中可以直接通过 `req.loginUser` 获取。
2. `ignore`: `String[]` 配置忽略登录校验的接口地址，这个配置在配合上本身 `fastify` 的机制，可以做到更精确的校验。例如：

   ```javascript
   // 注册路由
   app.register(async (Server) => {
     Server.register(
       async (ChildServer) => {
         // 加入登录验证
         ChildServer.register(require('fastify-auth-verify'), {
           verify(session) {
             return session.uid;
           },
           ignore: ['/api/login'],
         });
         ChildServer.register(require('./routes/api'));
       },
       { prefix: '/api' },
     );
     Server.register(require('./routes/root'));
   });
   ```

   上面的配置，`routes/root` 下面的接口本身就不会进行校验，只有 `routes/api` 下的接口才会进行校验，而在 `routes/api` 下的接口中，其中 `login` 接口忽略校验。
