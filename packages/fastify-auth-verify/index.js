const fp = require('fastify-plugin');

function auth(fastify, opts, done) {
  if (typeof opts.verify !== 'function') {
    opts.verify = () => {
      return true;
    };
  }
  opts.ignore = opts.ignore || [];
  // 保存 token 对应的登录后的信息包含 open_id 和 access_token
  fastify.decorateRequest('loginUser', null);

  fastify.addHook('onRequest', onRequest);

  done();

  function onRequest(req, reply, reqDone) {
    let url = new URL(`http://${req.headers.host}${req.url}`);
    if (opts.ignore.includes(url.pathname)) {
      reqDone();
      return;
    }
    let loginUser = opts.verify(req);
    if (loginUser == null) {
      const err = new Error('Unauthorized');
      err.statusCode = 401;
      reqDone(err);
    } else {
      req.loginUser = loginUser;
      loginUser = null;
      reqDone();
    }
  }
}

module.exports = fp(auth, { fastify: '3.x', name: 'fastify-authorization' });
